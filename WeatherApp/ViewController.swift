//
//  ViewController.swift
//  WeatherApp
//
//  Created by Yaroslav Georgievich on 02.04.2019.
//  Copyright © 2019 YP. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func refresh(_ sender: Any) {
        getWeather()
    }
    
    private let webServiceClient = WebServiceClient()
    private var weather = [List]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = 120
        tableView.dataSource = self
        
        getWeather()
    }

    func getWeather() {
        
        let kiev = "703448"
        let toronto = "6167865"
        let london = "4119617"
        
        guard let url = URL(string: "http://api.openweathermap.org/data/2.5/group?id=\(kiev),\(toronto),\(london)&units=metric&appid=a1d1dc41d71e2b1c1d329e64770bf088")
            else { return }
        let urlRequest = URLRequest(url: url)
        
        webServiceClient.fetchDataFromWeatherApi(url: urlRequest) { (weatherResponse, serviceError) in
            DispatchQueue.main.async {
                guard serviceError == nil else {
                    print("Service error when fetching the result: \(serviceError?.localizedDescription)")
                    return
                }
                guard let weatherResponse = weatherResponse else {
                    print("The data from server is nil")
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    let jsonData = try decoder.decode(Json.self, from: weatherResponse)
                    self.weather = jsonData.list
                    self.tableView.reloadData()
//                    dump(jsonData)
//                    print(self.weather[2].weather[0].icon)
                    print("call api")
                } catch {
                    print("Caught an excepsion: \(error.localizedDescription)")
                }
            }
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return weather.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherTableViewCell") as? WeatherTableViewCell else { return UITableViewCell() }
        
        cell.titleLabel.text = weather[indexPath.row].name
        
        let temp = Int(weather[indexPath.row].main.temp)
        let strTemp = String(temp)
        cell.tempLabel.text = strTemp
        
        cell.subtitleLabel.text = weather[indexPath.row].weather[0].description
        
        let icon = weather[indexPath.row].weather[0].icon
        if let imageUrl = URL(string: "http://openweathermap.org/img/w/\(icon).png") {
            let data = try? Data(contentsOf: imageUrl)
            if let data = data {
                let image = UIImage(data:data)
                DispatchQueue.main.async {
                    cell.imgView.image = image
                }
            }
        }
        
        return cell
    }
}
