//
//  Weather.swift
//  WeatherApp
//
//  Created by Yaroslav Georgievich on 01.04.2019.
//  Copyright © 2019 YP. All rights reserved.
//

import Foundation

struct Json: Codable {
    let cnt: Int
    let list: [List]
    
}
struct List: Codable {
    let weather: [Weather]
    let main: Main
    let name: String
}
struct Main: Codable {
    let temp: Float
}
struct Weather: Codable {
    let main: String
    let description: String
    let icon: String
}
